package wehyphens.com.ugo_driver.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import wehyphens.com.ugo_driver.R;
import wehyphens.com.ugo_driver.fragments.FragAccount;
import wehyphens.com.ugo_driver.fragments.FragEarning;
import wehyphens.com.ugo_driver.fragments.FragHome;
import wehyphens.com.ugo_driver.fragments.FragRating;

public class DashboardActivity extends AppCompatActivity {

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    private TextView mTextMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    Fragment FragHome=new FragHome();
                    fragmentManager=getSupportFragmentManager();
                    fragmentTransaction=fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container,FragHome);
                    fragmentTransaction.commit();

                    return true;
                case R.id.navigation_earning:

                    FragEarning fragEarning=new FragEarning();
                    fragmentManager=getSupportFragmentManager();
                    fragmentTransaction=fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container,fragEarning);
                    fragmentTransaction.commit();


                    return true;
                case R.id.navigation_rating:

                    FragRating fragRating=new FragRating();
                    fragmentManager=getSupportFragmentManager();
                    fragmentTransaction=fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container,fragRating);
                    fragmentTransaction.commit();


                    return true;

                case R.id.navigation_account:
                    FragAccount fragAccount=new FragAccount();
                    fragmentManager=getSupportFragmentManager();
                    fragmentTransaction=fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container,fragAccount);
                    fragmentTransaction.commit();

                    return true;

            }
            return false;
        }
    };


}
