package wehyphens.com.ugo_driver.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import wehyphens.com.ugo_driver.R;

public class FragRating extends Fragment {
    Context mContext;
    TextView tv_title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_rating, container, false);
        mContext=getActivity();

        tv_title=getActivity().findViewById(R.id.tv_title);
        tv_title.setText("OFFLINE");

        return view;
    }



}
